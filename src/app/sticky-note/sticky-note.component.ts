import { Component, OnInit, ViewChild } from '@angular/core';
import { MatMenuTrigger } from '@angular/material/menu';

@Component({
  selector: 'app-sticky-note',
  templateUrl: './sticky-note.component.html',
  styleUrls: ['./sticky-note.component.css']
})
export class StickyNoteComponent implements OnInit {

  stickys: Array<any> = [
    {
      text: 'New Sticky Text...'
    }
  ];
  @ViewChild(MatMenuTrigger)
  stickyMenu: MatMenuTrigger;

  stickyMenuPosition = { x: '0px', y: '0px' };
  zoomScale = false;

  onstickyMenu(event: any) {
    event.preventDefault();
    this.stickyMenuPosition.x = event.clientX + 'px';
    this.stickyMenuPosition.y = event.clientY + 'px';
    this.stickyMenu.openMenu();
  }
  constructor() { }

  ngOnInit(): void {
  }

  addNewSticky() {
    this.stickys.push({
      text: 'New Sticky Text...',
      editMode: false
    })
  }

  stopProp(event: Event) {
    event.stopPropagation();
  }

}
